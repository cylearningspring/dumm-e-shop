package io.gitlab.cylearningspring.dummeshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class DummEShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(DummEShopApplication.class, args);
	}

}
