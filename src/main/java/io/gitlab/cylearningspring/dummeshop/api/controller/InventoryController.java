package io.gitlab.cylearningspring.dummeshop.api.controller;

import io.gitlab.cylearningspring.dummeshop.api.service.InventoryService;
import io.gitlab.cylearningspring.dummeshop.persistence.entities.InventoryItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/inventory")
public class InventoryController {
    private final InventoryService inventoryService;

    @Autowired
    public InventoryController(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @GetMapping
    public Iterable<InventoryItem> getInventory() {
        return inventoryService.getInventoryItems();
    }

    @DeleteMapping("/{id}")
    public void deleteInventoryItem(@PathVariable Long id) {
        inventoryService.deleteInventoryItem(id);
    }

    @PostMapping
    public InventoryItem addInventoryItem(@RequestBody InventoryItem item) {
        return inventoryService.addInventoryItem(item);
    }

    @PutMapping
    public InventoryItem updateInventoryItem(@RequestBody InventoryItem item) {
        return inventoryService.updateInventoryItem(item);
    }

    @GetMapping("/{id}/{quantity}")
    public void incrementQuantity(@PathVariable Long id, @PathVariable int quantity) {
        inventoryService.adjustQuantity(id, quantity);
    }

    @DeleteMapping("/{id}/{quantity}")
    public void decrementQuantity(@PathVariable Long id, @PathVariable int quantity) {
        inventoryService.adjustQuantity(id, -quantity);
    }
}
