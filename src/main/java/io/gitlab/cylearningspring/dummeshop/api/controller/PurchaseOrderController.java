package io.gitlab.cylearningspring.dummeshop.api.controller;

import io.gitlab.cylearningspring.dummeshop.api.service.orders.PurchaseOrderService;
import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseOrder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@RestController
@RequestMapping("/orders")
public class PurchaseOrderController {
    private final PurchaseOrderService purchaseOrderService;

    @Autowired
    public PurchaseOrderController(PurchaseOrderService purchaseOrderService) {
        this.purchaseOrderService = purchaseOrderService;
    }

    @GetMapping
    public Iterable<PurchaseOrder> getOrders() {
        return purchaseOrderService.getAllOrders();
    }

    @GetMapping("/{id}")
    public Optional<PurchaseOrder> getPurchaseOrderById(@PathVariable Long id) {
        return purchaseOrderService.getPurchaseOrder(id);
    }

    @GetMapping("/forCustomer/{customerId}")
    public Iterable<PurchaseOrder> getPurchaseOrderByCustomerId(@PathVariable UUID customerId) {
        return purchaseOrderService.getPurchaseOrders(customerId);
    }

    @PostMapping
    public PurchaseOrder placeOrder(@RequestBody PurchaseOrder order) {
        return purchaseOrderService.placeOrder(order);
    }

    @DeleteMapping("/{id}")
    public void cancelOrder(@PathVariable Long id) {
        purchaseOrderService.cancelOrder(id);
    }

    @GetMapping("/{id}/pickup")
    public Optional<PurchaseOrder> pickupOrder(@PathVariable Long id) {
        return purchaseOrderService.pickupOrder(id);
    }
}
