package io.gitlab.cylearningspring.dummeshop.api.service;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.InventoryItem;
import io.gitlab.cylearningspring.dummeshop.persistence.repositories.InventoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class InventoryService {

    private final InventoryRepository inventoryRepository;

    @Autowired
    public InventoryService(InventoryRepository inventoryRepository) {
        this.inventoryRepository = inventoryRepository;
    }


    public Iterable<InventoryItem> getInventoryItems() {
        return inventoryRepository.findAll();
    }

    public void deleteInventoryItem(Long id) {
        log.debug("Deleting inventory item {}", id);
        inventoryRepository.deleteById(id);
    }

    public InventoryItem addInventoryItem(InventoryItem item) {
        return inventoryRepository.save(item);
    }

    public InventoryItem updateInventoryItem(InventoryItem item) {
        Long itemId = item.getId();
        Optional<InventoryItem> oldItemOptional = inventoryRepository.findById(itemId);
        if (oldItemOptional.isEmpty()) {
            throw new IllegalArgumentException("InventoryItem with id " + itemId + " doesn't exist!");
        }
        InventoryItem oldItem = oldItemOptional.get();
        oldItem.setProduct(item.getProduct());
        oldItem.setStockQuantity(item.getStockQuantity());

        return inventoryRepository.save(oldItem);
    }

    public void adjustQuantity(Long itemId, int quantity) {
        Optional<InventoryItem> oldItemOptional = inventoryRepository.findById(itemId);
        if (oldItemOptional.isEmpty()) {
            throw new IllegalArgumentException("InventoryItem with id " + itemId + " doesn't exist!");
        }
        InventoryItem item = oldItemOptional.get();
        item.setStockQuantity(item.getStockQuantity() + quantity);

        inventoryRepository.save(item);
    }
}
