package io.gitlab.cylearningspring.dummeshop.api.service;

import io.gitlab.cylearningspring.dummeshop.api.service.orders.PurchaseOrderService;
import io.gitlab.cylearningspring.dummeshop.persistence.entities.Product;
import io.gitlab.cylearningspring.dummeshop.persistence.repositories.ProductsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.StreamSupport;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProductsService {
    private final ProductsRepository productsRepository;
    private final PurchaseOrderService purchaseOrderService;

    @Autowired
    public ProductsService(ProductsRepository productsRepository, @Lazy PurchaseOrderService purchaseOrderService) {
        this.productsRepository = productsRepository;
        this.purchaseOrderService = purchaseOrderService;
    }

    public Iterable<Product> getAllProducts() {
        return productsRepository.findAll();
    }

    public Optional<Product> getProductById(Long id) {
        return productsRepository.findById(id);
    }

    public void deleteProduct(Long id) {
        boolean productUsedInOrder = StreamSupport.stream(purchaseOrderService.getAllOrders().spliterator(), false)
                .flatMap(o -> o.getOrderItems().stream())
                .anyMatch(pi -> id.equals(pi.getProduct().getId()));

        if (productUsedInOrder) {
            log.error("Product with id {} is used in an order. Cannot be deleted", id);
            throw new IllegalStateException("Product with id " + id + " is used in an order. Cannot be deleted");
        }

        productsRepository.deleteById(id);
    }

    public Product addProduct(Product product) {
        return productsRepository.save(product);
    }

    public Product updateProduct(Product product) {
        Long productId = product.getId();
        Optional<Product> oldProductOptional = productsRepository.findById(productId);
        if (oldProductOptional.isEmpty()) {
            throw new IllegalArgumentException("Product with id " + productId + " doesn't exist!");
        }
        Product oldProduct = oldProductOptional.get();
        oldProduct.setName(product.getName());
        oldProduct.setValue(product.getValue());
        return productsRepository.save(oldProduct);
    }
}
