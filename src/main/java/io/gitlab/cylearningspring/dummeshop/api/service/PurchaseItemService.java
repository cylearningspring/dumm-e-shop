package io.gitlab.cylearningspring.dummeshop.api.service;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.Product;
import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseItem;
import io.gitlab.cylearningspring.dummeshop.persistence.repositories.PurchaseItemRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.Optional;

import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
@Service
public class PurchaseItemService {
    private final PurchaseItemRepository purchaseItemRepository;
    private final ProductsService productsService;

    @Autowired
    public PurchaseItemService(PurchaseItemRepository purchaseItemRepository, @Lazy ProductsService productsService) {
        this.purchaseItemRepository = purchaseItemRepository;
        this.productsService = productsService;
    }

    public PurchaseItem persist(PurchaseItem item) {
        final Long productId = item.getProduct().getId();

        final Optional<Product> optionalProduct = productsService.getProductById(productId);
        if (optionalProduct.isEmpty()) {
            log.error("Cannot add item in order for product with id {}", productId);
            throw new IllegalArgumentException("No product found with id " + productId);
        }
        item.setProduct(optionalProduct.get());

        return purchaseItemRepository.save(item);
    }
}
