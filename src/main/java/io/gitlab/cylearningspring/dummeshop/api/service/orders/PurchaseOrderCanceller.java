package io.gitlab.cylearningspring.dummeshop.api.service.orders;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public interface PurchaseOrderCanceller {
    void cancel(Long id);
}
