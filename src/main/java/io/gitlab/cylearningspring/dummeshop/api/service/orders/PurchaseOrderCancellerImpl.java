package io.gitlab.cylearningspring.dummeshop.api.service.orders;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseOrder;
import io.gitlab.cylearningspring.dummeshop.persistence.repositories.PurchaseOrderRepository;
import io.gitlab.cylearningspring.dummeshop.processing.util.PurchaseOrderStatusUpdater;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseOrder.Status.CANCELED;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Service
public class PurchaseOrderCancellerImpl implements PurchaseOrderCanceller {
    private final PurchaseOrderRepository purchaseOrderRepository;
    private final PurchaseOrderStatusUpdater purchaseOrderStatusUpdater;

    @Autowired
    public PurchaseOrderCancellerImpl(PurchaseOrderRepository purchaseOrderRepository, PurchaseOrderStatusUpdater purchaseOrderStatusUpdater) {
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.purchaseOrderStatusUpdater = purchaseOrderStatusUpdater;
    }

    @Override
    public void cancel(Long id) {
        final Optional<PurchaseOrder> optionalPurchaseOrder = purchaseOrderRepository.findById(id);
        if (optionalPurchaseOrder.isEmpty()) {
            throw new IllegalArgumentException("No order found with id " + id);
        }

        final PurchaseOrder purchaseOrder = optionalPurchaseOrder.get();
        purchaseOrderStatusUpdater.updateStatus(purchaseOrder, CANCELED);

        // TODO De-allocate items
    }
}
