package io.gitlab.cylearningspring.dummeshop.api.service.orders;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseOrder;
import io.gitlab.cylearningspring.dummeshop.persistence.repositories.PurchaseOrderRepository;
import io.gitlab.cylearningspring.dummeshop.processing.PurchaseOrderRegister;
import io.gitlab.cylearningspring.dummeshop.processing.util.PurchaseOrderStatusUpdater;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Service
public class PurchaseOrderService {
    private final PurchaseOrderRepository purchaseOrderRepository;
    private final PurchaseOrderCanceller purchaseOrderCanceller;
    private final PurchaseOrderRegister purchaseOrderRegister;
    private final PurchaseOrderStatusUpdater purchaseOrderStatusUpdater;

    @Autowired
    public PurchaseOrderService(PurchaseOrderRepository purchaseOrderRepository,
                                PurchaseOrderCanceller purchaseOrderCanceller,
                                @Lazy PurchaseOrderRegister purchaseOrderRegister,
                                PurchaseOrderStatusUpdater purchaseOrderStatusUpdater) {
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.purchaseOrderCanceller = purchaseOrderCanceller;
        this.purchaseOrderRegister = purchaseOrderRegister;
        this.purchaseOrderStatusUpdater = purchaseOrderStatusUpdater;
    }

    public Iterable<PurchaseOrder> getAllOrders() {
        return purchaseOrderRepository.findAll();
    }

    public Optional<PurchaseOrder> getPurchaseOrder(Long id) {
        return purchaseOrderRepository.findById(id);
    }

    public Iterable<PurchaseOrder> getPurchaseOrders(UUID customerId) {
        return purchaseOrderRepository.findAllByCustomerId(customerId);
    }

    public PurchaseOrder placeOrder(PurchaseOrder order) {
        return purchaseOrderRegister.placeOrder(order);
    }

    public void cancelOrder(Long id) {
        purchaseOrderCanceller.cancel(id);
    }

    public Optional<PurchaseOrder> pickupOrder(Long id) {
        final Optional<PurchaseOrder> optionalOrder = purchaseOrderRepository.findById(id);

        if (optionalOrder.isEmpty()) {
            return Optional.empty();
        }

        final PurchaseOrder order = optionalOrder.get();

        return Optional.of(purchaseOrderStatusUpdater.updateStatus(order, PurchaseOrder.Status.COMPLETED));
    }
}
