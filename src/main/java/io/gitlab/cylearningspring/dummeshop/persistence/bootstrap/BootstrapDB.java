package io.gitlab.cylearningspring.dummeshop.persistence.bootstrap;

import io.gitlab.cylearningspring.dummeshop.persistence.repositories.CustomerRepository;
import io.gitlab.cylearningspring.dummeshop.persistence.repositories.InventoryRepository;
import io.gitlab.cylearningspring.dummeshop.persistence.repositories.ProductsRepository;
import io.gitlab.cylearningspring.dummeshop.persistence.repositories.PurchaseOrderRepository;

import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import javax.sql.DataSource;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.Customer;
import io.gitlab.cylearningspring.dummeshop.persistence.entities.InventoryItem;
import io.gitlab.cylearningspring.dummeshop.persistence.entities.Product;
import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseItem;
import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseOrder;
import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Slf4j
@Component
@Profile("bootstrap")
public class BootstrapDB implements CommandLineRunner {
    private final ProductsRepository productsRepository;
    private final CustomerRepository customerRepository;
    private final PurchaseOrderRepository purchaseOrderRepository;
    private final InventoryRepository inventoryRepository;

    @Autowired
    DataSource dataSource;

    @Autowired
    public BootstrapDB(ProductsRepository productsRepository,
                       CustomerRepository customerRepository,
                       PurchaseOrderRepository purchaseOrderRepository,
                       InventoryRepository inventoryRepository) {
        this.productsRepository = productsRepository;
        this.customerRepository = customerRepository;
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.inventoryRepository = inventoryRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        insertProducts();
        insertCustomers();
        insertInventory();
        insertOrders();

        MetadataSources metadata = new MetadataSources(
            new StandardServiceRegistryBuilder()
                .applySetting("hibernate.dialect", "org.hibernate.dialect.H2Dialect")
                .applySetting("javax.persistence.schema-generation-connection", dataSource.getConnection())
                .applySetting("hibernate.id.new_generator_mappings", "false")
                .build()
        );

        metadata.addAnnotatedClass(Customer.class);
        metadata.addAnnotatedClass(InventoryItem.class);
        metadata.addAnnotatedClass(Product.class);
        metadata.addAnnotatedClass(PurchaseItem.class);
        metadata.addAnnotatedClass(PurchaseOrder.class);

        SchemaExport export = new SchemaExport();
        export.setOutputFile("create-schema.sql");
        export.setDelimiter(";");
        export.create(EnumSet.of(TargetType.SCRIPT), metadata.buildMetadata());

        log.info("Exported create-schema.sql");
    }

    private void insertProducts() {
        productsRepository.save(Product.get("Corn flakes", BigDecimal.valueOf(3.45)));
        productsRepository.save(Product.get("Chocolate bar", BigDecimal.valueOf(0.65)));
        productsRepository.save(Product.get("Whiskey", BigDecimal.valueOf(24.99)));
    }

    private void insertCustomers() {
        customerRepository.save(Customer.get("Kypros", "Chrys", "kyprosc", "kypros@domain.com"));
        customerRepository.save(Customer.get("Elena", "Toum", "toumele", "toumele@domain.com"));
    }

    private void insertInventory() {
        productsRepository.findAll().forEach(product -> inventoryRepository.save(InventoryItem.get(product, product.getId() * 3)));
    }

    private void insertOrders() {
        Customer customer = customerRepository.findAll().iterator().next();

        final List<PurchaseItem> purchaseItems = new ArrayList<>();
        purchaseItems.add(PurchaseItem.get(productsRepository.findById(1L).get(), 1));
        purchaseItems.add(PurchaseItem.get(productsRepository.findById(2L).get(), 5));

        purchaseOrderRepository.save(PurchaseOrder.get(customer, purchaseItems, PurchaseOrder.Status.RECEIVED));
    }
}
