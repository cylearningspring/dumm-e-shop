package io.gitlab.cylearningspring.dummeshop.persistence.entities;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Entity
public class Customer {

    @Id
    @GeneratedValue
    private UUID id;

    @Column
    private String usertName;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String email;

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getUsertName() {
        return usertName;
    }

    public void setUsertName(String usertName) {
        this.usertName = usertName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static Customer get(String firstName, String lastName, String userName, String email) {
        final Customer customer = new Customer();
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setUsertName(userName);
        customer.setEmail(email);
        return customer;
    }
}
