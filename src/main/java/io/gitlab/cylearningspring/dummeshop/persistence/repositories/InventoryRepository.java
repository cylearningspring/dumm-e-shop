package io.gitlab.cylearningspring.dummeshop.persistence.repositories;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.InventoryItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryRepository extends CrudRepository<InventoryItem, Long> {

}
