package io.gitlab.cylearningspring.dummeshop.persistence.repositories;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.Product;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Repository
public interface ProductsRepository extends CrudRepository<Product, Long> {

}
