package io.gitlab.cylearningspring.dummeshop.persistence.repositories;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseItemRepository extends CrudRepository<PurchaseItem, Long> {

}
