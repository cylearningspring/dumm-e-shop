package io.gitlab.cylearningspring.dummeshop.persistence.repositories;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseOrder;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Repository
public interface PurchaseOrderRepository extends CrudRepository<PurchaseOrder, Long> {

    Iterable<PurchaseOrder> findAllByCustomerId(UUID customerId);
}
