package io.gitlab.cylearningspring.dummeshop.processing;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseOrder;
import org.springframework.integration.handler.GenericHandler;
import org.springframework.messaging.MessageHeaders;

/**
 * Notify the customer that they have a package ready to be picked up
 */
public interface CustomerNotifier extends GenericHandler<PurchaseOrder> {

    @Override
    Object handle(PurchaseOrder order, MessageHeaders headers);
}
