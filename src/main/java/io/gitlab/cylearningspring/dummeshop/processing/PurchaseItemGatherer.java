package io.gitlab.cylearningspring.dummeshop.processing;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseOrder;
import org.springframework.integration.handler.GenericHandler;
import org.springframework.messaging.MessageHeaders;

/**
 * Check for product stock and allocate needed if satisfiable
 */
public interface PurchaseItemGatherer extends GenericHandler<PurchaseOrder> {
    @Override
    Object handle(PurchaseOrder order, MessageHeaders headers);
}
