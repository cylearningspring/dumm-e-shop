package io.gitlab.cylearningspring.dummeshop.processing;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseOrder;
import org.springframework.integration.handler.GenericHandler;
import org.springframework.messaging.MessageHeaders;

/**
 * Package the requested products (and prepare for take away)
 */
public interface PurchaseOrderPackager extends GenericHandler<PurchaseOrder> {
    @Override
    Object handle(PurchaseOrder order, MessageHeaders headers);
}
