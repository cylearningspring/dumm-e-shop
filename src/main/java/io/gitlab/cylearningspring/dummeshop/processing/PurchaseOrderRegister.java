package io.gitlab.cylearningspring.dummeshop.processing;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseOrder;

/**
 * Registers a new purchase order for processing
 */
public interface PurchaseOrderRegister {
    PurchaseOrder placeOrder(PurchaseOrder order);
}
