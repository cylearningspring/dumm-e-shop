package io.gitlab.cylearningspring.dummeshop.processing.config;

import io.gitlab.cylearningspring.dummeshop.processing.CustomerNotifier;
import io.gitlab.cylearningspring.dummeshop.processing.PurchaseItemGatherer;
import io.gitlab.cylearningspring.dummeshop.processing.PurchaseOrderPackager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.MessageChannels;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.messaging.MessageChannel;

@Configuration
@EnableIntegration
public class ProcessingConfiguration {
    @Value("${processing.gather.poll.delay:5000}")
    private long purchaseGathererPollerDelay;

    //region Message Channels
    @Bean
    public MessageChannel purchaseOrderRegistrationOut() {
        return MessageChannels.queue().get();
    }
    //endregion

    @Bean
    public IntegrationFlow purchaseOrderProcessingFlow(
        MessageChannel purchaseOrderRegistrationOut,
        PurchaseItemGatherer purchaseItemGatherer,
        PurchaseOrderPackager purchaseOrderPackager,
        CustomerNotifier customerNotifier) {

        return IntegrationFlows.from(purchaseOrderRegistrationOut)
            .handle(purchaseItemGatherer, m -> m.poller(Pollers.fixedDelay(purchaseGathererPollerDelay)))
            .handle(purchaseOrderPackager)
            .handle(customerNotifier)
            .log(LoggingHandler.Level.DEBUG)
            .nullChannel();
    }
}
