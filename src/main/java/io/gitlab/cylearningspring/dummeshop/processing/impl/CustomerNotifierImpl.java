package io.gitlab.cylearningspring.dummeshop.processing.impl;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseOrder;
import io.gitlab.cylearningspring.dummeshop.processing.CustomerNotifier;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CustomerNotifierImpl implements CustomerNotifier {
    @Override
    public Object handle(PurchaseOrder order, MessageHeaders headers) {
        log.info("Notifying customer");
        return order;
    }
}
