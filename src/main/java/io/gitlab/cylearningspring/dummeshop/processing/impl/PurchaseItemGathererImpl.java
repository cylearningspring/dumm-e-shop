package io.gitlab.cylearningspring.dummeshop.processing.impl;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseOrder;
import io.gitlab.cylearningspring.dummeshop.processing.PurchaseItemGatherer;
import io.gitlab.cylearningspring.dummeshop.processing.util.PurchaseOrderStatusUpdater;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class PurchaseItemGathererImpl implements PurchaseItemGatherer {
    @Value("${processing.gather.time:2000}")
    private long processingTime;
    private final PurchaseOrderStatusUpdater purchaseOrderStatusUpdater;

    @Autowired
    public PurchaseItemGathererImpl(PurchaseOrderStatusUpdater purchaseOrderStatusUpdater) {
        this.purchaseOrderStatusUpdater = purchaseOrderStatusUpdater;
    }

    @Override
    public Object handle(PurchaseOrder order, MessageHeaders headers) {
        log.info("Gathering order items");

        try {
            // TODO Allocate order items from inventory
            Thread.sleep(processingTime);
        } catch (InterruptedException e) {
            // Do nothing
            log.warn("Interrupted!");
        }

        return purchaseOrderStatusUpdater.updateStatus(order, PurchaseOrder.Status.ALLOCATED);
    }
}
