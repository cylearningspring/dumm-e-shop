package io.gitlab.cylearningspring.dummeshop.processing.impl;

import io.gitlab.cylearningspring.dummeshop.api.service.PurchaseItemService;
import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseItem;
import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseOrder;
import io.gitlab.cylearningspring.dummeshop.persistence.repositories.PurchaseOrderRepository;
import io.gitlab.cylearningspring.dummeshop.processing.PurchaseOrderRegister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
//@ConditionalOnMissingBean(PurchaseOrderRegister.class)
public class PurchaseOrderRegisterImpl implements PurchaseOrderRegister {
    private final PurchaseOrderRepository purchaseOrderRepository;
    private final PurchaseItemService purchaseItemService;

    private final MessageChannel outputChannel;

    @Autowired
    public PurchaseOrderRegisterImpl(PurchaseOrderRepository purchaseOrderRepository,
                                     PurchaseItemService purchaseItemService,
                                     @Qualifier("purchaseOrderRegistrationOut") MessageChannel outputChannel) {
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.purchaseItemService = purchaseItemService;
        this.outputChannel = outputChannel;
    }

    @Override
    public PurchaseOrder placeOrder(PurchaseOrder order) {
        if (null == order.getStatus()) {
            order.setStatus(PurchaseOrder.Status.RECEIVED);
        }

        final PurchaseOrder savedOrder = purchaseOrderRepository.save(order);

        // First persist the purchase items
        List<PurchaseItem> orderItems = savedOrder.getOrderItems();
        if (null != orderItems) {
            for (int i = 0; i < orderItems.size(); i++) {
                PurchaseItem item = orderItems.get(i);
                item.setOrder(savedOrder);
                PurchaseItem savedItem = purchaseItemService.persist(item);
                orderItems.set(i, savedItem);
            }
        }

        outputChannel.send(MessageBuilder.withPayload(savedOrder).build());

        return savedOrder;
    }
}
