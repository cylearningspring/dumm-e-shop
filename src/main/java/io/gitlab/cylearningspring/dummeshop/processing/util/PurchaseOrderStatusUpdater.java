package io.gitlab.cylearningspring.dummeshop.processing.util;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseOrder;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public interface PurchaseOrderStatusUpdater {
    /**
     * Updates the status of the given {@link PurchaseOrder} to the given status or throws an exception on error
     *
     * @param order The {@link PurchaseOrder} to be updated
     * @param status The status to set
     * @throws IllegalArgumentException If transitioning to the given status is not valid
     *
     * @return The updated PurchaseOrder
     */
    PurchaseOrder updateStatus(PurchaseOrder order, PurchaseOrder.Status status);
}
