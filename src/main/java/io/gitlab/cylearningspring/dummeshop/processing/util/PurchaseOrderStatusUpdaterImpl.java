package io.gitlab.cylearningspring.dummeshop.processing.util;

import io.gitlab.cylearningspring.dummeshop.persistence.entities.PurchaseOrder;
import io.gitlab.cylearningspring.dummeshop.persistence.repositories.PurchaseOrderRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
@Service
public class PurchaseOrderStatusUpdaterImpl implements PurchaseOrderStatusUpdater {
    private final PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    public PurchaseOrderStatusUpdaterImpl(PurchaseOrderRepository purchaseOrderRepository) {
        this.purchaseOrderRepository = purchaseOrderRepository;
    }

    @Override
    public PurchaseOrder updateStatus(PurchaseOrder order, PurchaseOrder.Status status) {
        verifyStatusTransition(order, status);

        order.setStatus(status);
        return purchaseOrderRepository.save(order);
    }

    @SuppressWarnings("java:S1199")
    private void verifyStatusTransition(PurchaseOrder order, PurchaseOrder.Status status) {
        final String purchaseOrderIdStr = "Purchase Order " + order.getId();
        final PurchaseOrder.Status currentStatus = order.getStatus();
        switch (status) {
            case COMPLETED -> {
                if (!PurchaseOrder.Status.READY.equals(currentStatus)) {
                    throw new IllegalArgumentException(purchaseOrderIdStr + " is not ready yet");
                }
            }
            case RECEIVED -> throw new IllegalArgumentException("Cannot return " + purchaseOrderIdStr + " back to received state");
            case ALLOCATED -> {
                if (!PurchaseOrder.Status.RECEIVED.equals(currentStatus)) {
                    throw new IllegalArgumentException(purchaseOrderIdStr + " items have already been allocated");
                }
            }
            case READY -> {
                if (!PurchaseOrder.Status.ALLOCATED.equals(currentStatus)) {
                    if (PurchaseOrder.Status.RECEIVED.equals(currentStatus)) {
                        throw new IllegalArgumentException(purchaseOrderIdStr + " items have not been allocated yet");
                    } else {
                        throw new IllegalArgumentException(purchaseOrderIdStr + " is already ready");
                    }
                }
            }
            case CANCELED -> {
                if (PurchaseOrder.Status.COMPLETED.equals(currentStatus)) {
                    throw new IllegalArgumentException(purchaseOrderIdStr + " has already been completed and picked up");
                } else if (PurchaseOrder.Status.CANCELED.equals(currentStatus)) {
                    throw new IllegalArgumentException(purchaseOrderIdStr + " has already been canceled");
                }
            }
            default -> {
                // No default checks
            }
        }
    }
}
